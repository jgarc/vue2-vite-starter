# Vue 2 + Vite

This template should help get you started developing with Vue 2 in Vite. The project was created using the CLI command found in the [Vite documentation](https://vitejs.dev/guide/#scaffolding-your-first-vite-project)

```bash
npm create vite@latest nuxt2-vite-starter -- --template vue
```
I then adjusted the dependancies to include the latest Vue 2 release, and adjusted some boilerplate to support this version. Note that this Vue 2 version supports using the composition API, which was [ported to work with this release of Vue 2.7](https://v2.vuejs.org/v2/guide/migration-vue-2-7). The template uses `<script setup>` SFCs. Check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

